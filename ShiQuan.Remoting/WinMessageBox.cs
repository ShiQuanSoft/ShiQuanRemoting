﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Remoting
{
    /// <summary>
    /// 
    /// </summary>
    public static class WinMessageBox
    {
        /// <summary>
        /// 系统提示
        /// </summary>
        /// <param name="pMsg">信息</param>
        public static DialogResult Show(string pMsg)
        {
            return MessageBox.Show(pMsg, "系统提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="pMsg">信息</param>
        public static DialogResult ShowWarning(string pMsg)
        {
            return MessageBox.Show(pMsg, "警告信息", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        /// <summary>
        /// 系统异常
        /// </summary>
        /// <param name="pMsg">信息</param>
        public static DialogResult ShowError(string pMsg)
        {
            return MessageBox.Show(pMsg, "系统异常", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        /// <summary>
        /// 确认信息
        /// </summary>
        /// <param name="pMsg">信息</param>
        /// <returns></returns>
        public static bool ShowQuestion(string pMsg)
        {
            return (MessageBox.Show(pMsg, "确认信息", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK);
        }
    }
}
