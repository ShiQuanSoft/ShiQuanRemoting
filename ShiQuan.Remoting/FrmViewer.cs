﻿using ShiQuan.Remoting.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Remoting
{
    public class FrmViewer : Form
    {
        private AxRDPCOMAPILib.AxRDPViewer pRdpViewer;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmViewer));
            this.pRdpViewer = new AxRDPCOMAPILib.AxRDPViewer();
            ((System.ComponentModel.ISupportInitialize)(this.pRdpViewer)).BeginInit();
            this.SuspendLayout();
            // 
            // pRdpViewer
            // 
            this.pRdpViewer.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.pRdpViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pRdpViewer.Enabled = true;
            this.pRdpViewer.Location = new System.Drawing.Point(0, 0);
            this.pRdpViewer.Name = "pRdpViewer";
            this.pRdpViewer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("pRdpViewer.OcxState")));
            this.pRdpViewer.Size = new System.Drawing.Size(729, 438);
            this.pRdpViewer.TabIndex = 1;
            // 
            // FrmViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 438);
            this.Controls.Add(this.pRdpViewer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmViewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmViewer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pRdpViewer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public FrmViewer()
        {
            InitializeComponent();

            this.Load += FrmViewer_Load;
        }
        /// <summary>
        /// 远程连接字符串
        /// </summary>
        public string ConnectionString
        {
            get;
            set;
        }

        void FrmViewer_Load(object sender, EventArgs e)
        {
            try
            {
                this.pRdpViewer.OnConnectionEstablished += pRdpViewer_OnConnectionEstablished;
                this.pRdpViewer.OnConnectionFailed += pRdpViewer_OnConnectionFailed;
                this.pRdpViewer.OnConnectionTerminated += pRdpViewer_OnConnectionTerminated;
                this.pRdpViewer.OnError += pRdpViewer_OnError;
                Logger.Info("开始连接：" + ConnectionString);
                this.pRdpViewer.Connect(ConnectionString, "Viewer1", "");
                //请求控制
                this.pRdpViewer.RequestControl(RDPCOMAPILib.CTRL_LEVEL.CTRL_LEVEL_INTERACTIVE);
                Logger.Info("连接成功");
                this.FormClosing += FrmViewer_FormClosing;
            }
            catch (Exception ex)
            {
                Logger.Info("连接异常："+ex.ToString());
                WinMessageBox.ShowError("连接异常："+ex.Message);
            }
        }

        void FrmViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                pRdpViewer.Disconnect();
            }
            catch (Exception ex)
            {
                Logger.Info("关闭连接异常：" + ex.ToString());
            }
        }

        void pRdpViewer_OnError(object sender, AxRDPCOMAPILib._IRDPSessionEvents_OnErrorEvent e)
        {
            int ErrorCode = (int)e.errorInfo;
            Logger.Info("Error 0x" + ErrorCode.ToString("X"));
        }

        void pRdpViewer_OnConnectionTerminated(object sender, AxRDPCOMAPILib._IRDPSessionEvents_OnConnectionTerminatedEvent e)
        {
            Logger.Info("Connection Terminated. Reason: " + e.discReason);
        }

        void pRdpViewer_OnConnectionFailed(object sender, EventArgs e)
        {
            Logger.Info("Connection Failed.");
        }

        void pRdpViewer_OnConnectionEstablished(object sender, EventArgs e)
        {
            Logger.Info("ConnectionEstablished.");
        }
    }
}
