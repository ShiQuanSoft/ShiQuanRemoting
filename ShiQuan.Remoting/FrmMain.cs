﻿using ShiQuan.WinControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Remoting
{
    public class FrmMain : FrmBasic
    {
        private UCStatusBar ucStatusBar;
        private UCToolBar ucToolBar;
        private ToolStripStatusLabel sysStatusLabel;
        private ToolStripDropDownButton tbbHelper;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private ToolStripMenuItem mnuAbout;
        private RemoteControls.UCRemoteMachine ucRemoteMachine1;

        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.ucStatusBar = new ShiQuan.WinControls.UCStatusBar();
            this.sysStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ucToolBar = new ShiQuan.WinControls.UCToolBar();
            this.tbbHelper = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ucRemoteMachine1 = new ShiQuan.RemoteControls.UCRemoteMachine();
            this.ucStatusBar.SuspendLayout();
            this.ucToolBar.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ucPanelTitle1
            // 
            this.ucPanelTitle1.Caption = "FrmBasic";
            this.ucPanelTitle1.Size = new System.Drawing.Size(800, 27);
            // 
            // ucStatusBar
            // 
            this.ucStatusBar.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(209)))), ((int)(((byte)(220)))));
            this.ucStatusBar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(144)))), ((int)(((byte)(165)))));
            this.ucStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sysStatusLabel});
            this.ucStatusBar.Location = new System.Drawing.Point(0, 574);
            this.ucStatusBar.Name = "ucStatusBar";
            this.ucStatusBar.Size = new System.Drawing.Size(800, 26);
            this.ucStatusBar.TabIndex = 1;
            this.ucStatusBar.Text = "statusStrip1";
            // 
            // sysStatusLabel
            // 
            this.sysStatusLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)));
            this.sysStatusLabel.Name = "sysStatusLabel";
            this.sysStatusLabel.Size = new System.Drawing.Size(785, 21);
            this.sysStatusLabel.Spring = true;
            this.sysStatusLabel.Text = "技术支持：896374871@qq.com";
            this.sysStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ucToolBar
            // 
            this.ucToolBar.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(209)))), ((int)(((byte)(220)))));
            this.ucToolBar.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(144)))), ((int)(((byte)(165)))));
            this.ucToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbHelper});
            this.ucToolBar.Location = new System.Drawing.Point(0, 27);
            this.ucToolBar.Name = "ucToolBar";
            this.ucToolBar.Size = new System.Drawing.Size(800, 25);
            this.ucToolBar.TabIndex = 2;
            this.ucToolBar.Text = "toolStrip1";
            // 
            // tbbHelper
            // 
            this.tbbHelper.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tbbHelper.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbout});
            this.tbbHelper.Image = global::ShiQuan.Remoting.Properties.Resources.btnRefresh;
            this.tbbHelper.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbHelper.Name = "tbbHelper";
            this.tbbHelper.Size = new System.Drawing.Size(61, 22);
            this.tbbHelper.Text = "帮助";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(100, 22);
            this.mnuAbout.Text = "关于";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 52);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 522);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(209)))), ((int)(((byte)(220)))));
            this.tabPage1.Controls.Add(this.ucRemoteMachine1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(792, 496);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "远程桌面";
            // 
            // ucRemoteMachine1
            // 
            this.ucRemoteMachine1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(197)))), ((int)(((byte)(209)))), ((int)(((byte)(220)))));
            this.ucRemoteMachine1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucRemoteMachine1.Location = new System.Drawing.Point(0, 0);
            this.ucRemoteMachine1.Name = "ucRemoteMachine1";
            this.ucRemoteMachine1.Size = new System.Drawing.Size(792, 496);
            this.ucRemoteMachine1.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ucToolBar);
            this.Controls.Add(this.ucStatusBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.Controls.SetChildIndex(this.ucPanelTitle1, 0);
            this.Controls.SetChildIndex(this.ucStatusBar, 0);
            this.Controls.SetChildIndex(this.ucToolBar, 0);
            this.Controls.SetChildIndex(this.tabControl1, 0);
            this.ucStatusBar.ResumeLayout(false);
            this.ucStatusBar.PerformLayout();
            this.ucToolBar.ResumeLayout(false);
            this.ucToolBar.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public FrmMain()
        {
            InitializeComponent();

            this.Size = new Size(1024, 720);
            this.Text = Application.ProductName + "("+Application.ProductVersion+")";
            this.Load += FrmMain_Load;
        }

        void FrmMain_Load(object sender, EventArgs e)
        {
            this.mnuAbout.Click += mnuAbout_Click;
        }

        void mnuAbout_Click(object sender, EventArgs e)
        {
            new AboutBox().ShowDialog();
        }
    }
}
