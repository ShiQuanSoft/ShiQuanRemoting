﻿using ShiQuan.Remoting.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Remoting
{
    public class FrmMstsc : Form
    {
        private AxMSTSCLib.AxMsRdpClient7 axMsRdpClient71;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMstsc));
            this.axMsRdpClient71 = new AxMSTSCLib.AxMsRdpClient7();
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient71)).BeginInit();
            this.SuspendLayout();
            // 
            // axMsRdpClient71
            // 
            this.axMsRdpClient71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axMsRdpClient71.Enabled = true;
            this.axMsRdpClient71.Location = new System.Drawing.Point(0, 0);
            this.axMsRdpClient71.Name = "axMsRdpClient71";
            this.axMsRdpClient71.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axMsRdpClient71.OcxState")));
            this.axMsRdpClient71.Size = new System.Drawing.Size(724, 466);
            this.axMsRdpClient71.TabIndex = 1;
            // 
            // FrmMstsc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 466);
            this.Controls.Add(this.axMsRdpClient71);
            this.Name = "FrmMstsc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmMstsc";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.axMsRdpClient71)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        /// <summary>
        /// 远程主机信息
        /// </summary>
        public RemoteMachine RemoteMachine
        {
            get;
            set;
        }
        public FrmMstsc()
        {
            InitializeComponent();

            this.Load += FrmMstsc_Load;
        }

        void FrmMstsc_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = this.RemoteMachine.Name;

                this.axMsRdpClient71.Server = this.RemoteMachine.Server;//主机121.36.138.125
                this.axMsRdpClient71.AdvancedSettings2.RDPPort = Convert.ToInt32(this.RemoteMachine.Port);//端口号3389
                this.axMsRdpClient71.UserName = this.RemoteMachine.Account;//账号-administrator
                this.axMsRdpClient71.AdvancedSettings2.ClearTextPassword = this.RemoteMachine.Password;//密码-QDCLS@2020

                this.axMsRdpClient71.DesktopWidth = this.Width;
                this.axMsRdpClient71.DesktopHeight = this.Height - 60;//减状态栏高度

                this.axMsRdpClient71.OnConnected += axMsRdpClient71_OnConnected;
                this.axMsRdpClient71.OnEnterFullScreenMode += axMsRdpClient71_OnEnterFullScreenMode;
                this.axMsRdpClient71.OnAuthenticationWarningDismissed += axMsRdpClient71_OnAuthenticationWarningDismissed;

                this.axMsRdpClient71.OnDisconnected += axMsRdpClient71_OnDisconnected;
                
                this.axMsRdpClient71.Connect();
                Logger.Info("连接桌面成功："+this.axMsRdpClient71.Server);
                this.FormClosing += FrmMstsc_FormClosing;
            }
            catch (Exception ex)
            {
                Logger.Info("连接桌面异常:" + ex.ToString());
                WinMessageBox.ShowError("连接桌面异常：" + ex.Message);
            }
        }

        void axMsRdpClient71_OnEnterFullScreenMode(object sender, EventArgs e)
        {
            Logger.Info(" EnterFullScreenMode!");
        }

        void axMsRdpClient71_OnDisconnected(object sender, AxMSTSCLib.IMsTscAxEvents_OnDisconnectedEvent e)
        {
            Logger.Info(" Disconnected!");
        }

        void axMsRdpClient71_OnConnected(object sender, EventArgs e)
        {
            Logger.Info(" Connected!");
        }


        void axMsRdpClient71_OnAuthenticationWarningDismissed(object sender, EventArgs e)
        {
            Logger.Info(" AuthenticationWarningDismissed!");
        }

        void FrmMstsc_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.axMsRdpClient71.Disconnect();
                this.axMsRdpClient71.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Info("关闭连接异常：" + ex.ToString());
            }
        }
    }
}
