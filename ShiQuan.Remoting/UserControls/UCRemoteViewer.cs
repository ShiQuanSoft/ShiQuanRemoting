﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Remoting
{
    public class UCRemoteViewer : UserControl
    {

        private System.Windows.Forms.ToolStrip sysToolBar;
        private System.Windows.Forms.ToolStripButton tbbConnect;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;

        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCRemoteViewer));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.tbbConnect = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sysToolBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbConnect,
            this.toolStripSeparator1});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(746, 25);
            this.sysToolBar.TabIndex = 1;
            // 
            // tbbConnect
            // 
            this.tbbConnect.Image = ((System.Drawing.Image)(resources.GetObject("tbbConnect.Image")));
            this.tbbConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbConnect.Name = "tbbConnect";
            this.tbbConnect.Size = new System.Drawing.Size(49, 22);
            this.tbbConnect.Text = "连接";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // UCRemoteViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sysToolBar);
            this.Name = "UCRemoteViewer";
            this.Size = new System.Drawing.Size(746, 459);
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        public UCRemoteViewer()
        {
            InitializeComponent();

            this.Load += UCRemoteViewer_Load;
        }

        void UCRemoteViewer_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
                return;

            this.tbbConnect.Click += tbbConnect_Click;
        }

        void tbbConnect_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "远程主机(*.xml)|*.xml";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;
            string fileContent = System.IO.File.ReadAllText(dialog.FileName);
            FrmViewer frm = new FrmViewer();
            frm.Show();
        }
    }
}
