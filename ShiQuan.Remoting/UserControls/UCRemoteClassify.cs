﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShiQuan.Remoting.UserControls
{
    public partial class UCRemoteClassify : UserControl
    {
        private System.Windows.Forms.ToolStrip sysToolBar;
        private System.Windows.Forms.ToolStripDropDownButton tbbAdd;
        private System.Windows.Forms.ToolStripMenuItem mnuAddClassify;
        private System.Windows.Forms.ToolStripMenuItem mnuSubClassify;
        private System.Windows.Forms.ToolStripButton tbbCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbbModify;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbbDelete;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ImageList imageList1;

        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCRemoteClassify));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.tbbAdd = new System.Windows.Forms.ToolStripDropDownButton();
            this.mnuAddClassify = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSubClassify = new System.Windows.Forms.ToolStripMenuItem();
            this.tbbCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbModify = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbDelete = new System.Windows.Forms.ToolStripButton();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.sysToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbAdd,
            this.tbbCopy,
            this.toolStripSeparator2,
            this.tbbModify,
            this.toolStripSeparator1,
            this.tbbDelete});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(738, 25);
            this.sysToolBar.TabIndex = 1;
            this.sysToolBar.Text = "toolStrip1";
            // 
            // tbbAdd
            // 
            this.tbbAdd.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAddClassify,
            this.mnuSubClassify});
            this.tbbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbbAdd.Image")));
            this.tbbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbAdd.Name = "tbbAdd";
            this.tbbAdd.Size = new System.Drawing.Size(58, 22);
            this.tbbAdd.Text = "新增";
            // 
            // mnuAddClassify
            // 
            this.mnuAddClassify.Name = "mnuAddClassify";
            this.mnuAddClassify.Size = new System.Drawing.Size(152, 22);
            this.mnuAddClassify.Text = "新增大类";
            // 
            // mnuSubClassify
            // 
            this.mnuSubClassify.Name = "mnuSubClassify";
            this.mnuSubClassify.Size = new System.Drawing.Size(152, 22);
            this.mnuSubClassify.Text = "新增子类";
            // 
            // tbbCopy
            // 
            this.tbbCopy.Image = ((System.Drawing.Image)(resources.GetObject("tbbCopy.Image")));
            this.tbbCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbCopy.Name = "tbbCopy";
            this.tbbCopy.Size = new System.Drawing.Size(67, 22);
            this.tbbCopy.Text = "复制(&C)";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbModify
            // 
            this.tbbModify.Image = ((System.Drawing.Image)(resources.GetObject("tbbModify.Image")));
            this.tbbModify.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbModify.Name = "tbbModify";
            this.tbbModify.Size = new System.Drawing.Size(67, 22);
            this.tbbModify.Text = "修改(&M)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbDelete
            // 
            this.tbbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbbDelete.Image")));
            this.tbbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbDelete.Name = "tbbDelete";
            this.tbbDelete.Size = new System.Drawing.Size(67, 22);
            this.tbbDelete.Text = "删除(&D)";
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(0, 25);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 1;
            this.treeView1.Size = new System.Drawing.Size(200, 462);
            this.treeView1.TabIndex = 2;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "FileClose.gif");
            this.imageList1.Images.SetKeyName(1, "FileOpen.gif");
            // 
            // splitter1
            // 
            this.splitter1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitter1.Location = new System.Drawing.Point(200, 25);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 462);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(205, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(533, 462);
            this.dataGridView1.TabIndex = 4;
            // 
            // UCRemoteClassify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.sysToolBar);
            this.Name = "UCRemoteClassify";
            this.Size = new System.Drawing.Size(738, 487);
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public UCRemoteClassify()
        {
            InitializeComponent();
        }
    }
}
