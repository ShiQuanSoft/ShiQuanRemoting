﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ShiQuan.Remoting.Helper;

namespace ShiQuan.Remoting
{
    public  class UCRemoteMachine : UserControl
    {
        private System.Windows.Forms.ToolStrip sysToolBar;
        private System.Windows.Forms.ToolStripButton tbbAdd;
        private System.Windows.Forms.ToolStripButton tbbCopy;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tbbModify;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tbbDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tbbConnect;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtName;
        private ToolStripSeparator toolStripSeparator4;
        private ToolStripButton tbbRef;

        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCRemoteMachine));
            this.sysToolBar = new System.Windows.Forms.ToolStrip();
            this.tbbAdd = new System.Windows.Forms.ToolStripButton();
            this.tbbCopy = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbModify = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbDelete = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbConnect = new System.Windows.Forms.ToolStripButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnQuery = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tbbRef = new System.Windows.Forms.ToolStripButton();
            this.sysToolBar.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // sysToolBar
            // 
            this.sysToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tbbAdd,
            this.tbbCopy,
            this.toolStripSeparator1,
            this.tbbModify,
            this.toolStripSeparator2,
            this.tbbDelete,
            this.toolStripSeparator3,
            this.tbbConnect,
            this.toolStripSeparator4,
            this.tbbRef});
            this.sysToolBar.Location = new System.Drawing.Point(0, 0);
            this.sysToolBar.Name = "sysToolBar";
            this.sysToolBar.Size = new System.Drawing.Size(781, 25);
            this.sysToolBar.TabIndex = 2;
            this.sysToolBar.Text = "toolStrip1";
            // 
            // tbbAdd
            // 
            this.tbbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tbbAdd.Image")));
            this.tbbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbAdd.Name = "tbbAdd";
            this.tbbAdd.Size = new System.Drawing.Size(67, 22);
            this.tbbAdd.Text = "新增(&A)";
            // 
            // tbbCopy
            // 
            this.tbbCopy.Image = ((System.Drawing.Image)(resources.GetObject("tbbCopy.Image")));
            this.tbbCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbCopy.Name = "tbbCopy";
            this.tbbCopy.Size = new System.Drawing.Size(67, 22);
            this.tbbCopy.Text = "复制(&C)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbModify
            // 
            this.tbbModify.Image = ((System.Drawing.Image)(resources.GetObject("tbbModify.Image")));
            this.tbbModify.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbModify.Name = "tbbModify";
            this.tbbModify.Size = new System.Drawing.Size(67, 22);
            this.tbbModify.Text = "修改(&M)";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbDelete
            // 
            this.tbbDelete.Image = ((System.Drawing.Image)(resources.GetObject("tbbDelete.Image")));
            this.tbbDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbDelete.Name = "tbbDelete";
            this.tbbDelete.Size = new System.Drawing.Size(67, 22);
            this.tbbDelete.Text = "删除(&D)";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbConnect
            // 
            this.tbbConnect.Image = ((System.Drawing.Image)(resources.GetObject("tbbConnect.Image")));
            this.tbbConnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbConnect.Name = "tbbConnect";
            this.tbbConnect.Size = new System.Drawing.Size(67, 22);
            this.tbbConnect.Text = "连接(&O)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.treeView1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 501);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "远程分组";
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(3, 17);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(194, 481);
            this.treeView1.TabIndex = 0;
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(200, 25);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 501);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(205, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(576, 501);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "远程列表";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 55);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(570, 443);
            this.dataGridView1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(570, 38);
            this.panel1.TabIndex = 1;
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(225, 3);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 23);
            this.btnQuery.TabIndex = 2;
            this.btnQuery.Text = "查询(&Q)";
            this.btnQuery.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "名称";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(69, 5);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(150, 21);
            this.txtName.TabIndex = 0;
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // tbbRef
            // 
            this.tbbRef.Image = ((System.Drawing.Image)(resources.GetObject("tbbRef.Image")));
            this.tbbRef.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tbbRef.Name = "tbbRef";
            this.tbbRef.Size = new System.Drawing.Size(67, 22);
            this.tbbRef.Text = "刷新(&R)";
            // 
            // UCRemoteMachine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.sysToolBar);
            this.Name = "UCRemoteMachine";
            this.Size = new System.Drawing.Size(781, 526);
            this.sysToolBar.ResumeLayout(false);
            this.sysToolBar.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public UCRemoteMachine()
        {
            InitializeComponent();

            this.Load += UCRemoteMachine_Load;
        }

        void UCRemoteMachine_Load(object sender, EventArgs e)
        {
            if (this.DesignMode)
                return;

            #region 列表初始
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            DataGridViewTextBoxColumn dc = null;
            dc = new DataGridViewTextBoxColumn();
            dc.Name = dc.DataPropertyName = "Name";
            dc.HeaderText = "名称";
            dc.Width = 100;
            this.dataGridView1.Columns.Add(dc);

            dc = new DataGridViewTextBoxColumn();
            dc.Name = dc.DataPropertyName = "Server";
            dc.HeaderText = "主机";
            dc.Width = 200;
            this.dataGridView1.Columns.Add(dc);

            dc = new DataGridViewTextBoxColumn();
            dc.Name = dc.DataPropertyName = "Port";
            dc.HeaderText = "端口";
            dc.Width = 60;
            this.dataGridView1.Columns.Add(dc);

            dc = new DataGridViewTextBoxColumn();
            dc.Name = dc.DataPropertyName = "Account";
            dc.HeaderText = "登录账号";
            dc.Width = 150;
            this.dataGridView1.Columns.Add(dc);
            #endregion

            this.LoadData();

            this.tbbAdd.Click += tbbAdd_Click;
            this.tbbCopy.Click += tbbCopy_Click;
            this.tbbModify.Click += tbbModify_Click;
            this.tbbDelete.Click += tbbDelete_Click;
            this.btnQuery.Click += btnQuery_Click;
            this.tbbConnect.Click += tbbConnect_Click;
            
            this.dataGridView1.CellDoubleClick += dataGridView1_CellDoubleClick;
        }

        void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.tbbConnect.PerformClick();
        }

        void tbbAdd_Click(object sender, EventArgs e)
        {
            FrmRemoteEdit frm = new FrmRemoteEdit();
            if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            this.LoadData();
        }
        void tbbDelete_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.CurrentCell == null)
                return;
            try
            {
                int rowIndex = this.dataGridView1.CurrentCell.RowIndex;
                RemoteMachine remote = this.dataGridView1.Rows[rowIndex].DataBoundItem as RemoteMachine;
                if (remote == null)
                {
                    return;
                }
                if (WinMessageBox.ShowQuestion("确认删除所选择的信息？") == false)
                {
                    return;
                }
                this.dataGridView1.CurrentCell = null;
                if (RemoteMachineHelper.Remove(remote.Id))
                {
                    //this.LoadData();
                    BindingSource bindingSource = dataGridView1.DataSource as BindingSource;
                    bindingSource.RemoveCurrent();

                    WinMessageBox.Show("删除成功！");
                }
            }
            catch (Exception ex)
            {
                WinMessageBox.ShowError("删除数据异常：" + ex.Message);
            }

        }

        void tbbModify_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.CurrentCell == null)
                return;
            int rowIndex = this.dataGridView1.CurrentCell.RowIndex;
            RemoteMachine remote = this.dataGridView1.Rows[rowIndex].DataBoundItem as RemoteMachine;
            if (remote == null)
            {
                return;
            }
            FrmRemoteEdit frm = new FrmRemoteEdit();
            frm.KeyValue = remote.Id;
            if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            this.LoadData();
        }

        void tbbCopy_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.CurrentCell == null)
                return;
            int rowIndex = this.dataGridView1.CurrentCell.RowIndex;
            RemoteMachine remote = this.dataGridView1.Rows[rowIndex].DataBoundItem as RemoteMachine;
            if (remote == null)
            {
                return;
            }
            FrmRemoteEdit frm = new FrmRemoteEdit();
            frm.CopyId = remote.Id;
            if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;
            this.LoadData();
        }

        void tbbConnect_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.CurrentCell == null)
                return;
            int rowIndex = this.dataGridView1.CurrentCell.RowIndex;
            RemoteMachine remote = this.dataGridView1.Rows[rowIndex].DataBoundItem as RemoteMachine;
            if (remote == null)
            {
                return;
            }
            FrmMstsc frm = new FrmMstsc();
            frm.RemoteMachine = remote;
            frm.Show();
        }

        void btnQuery_Click(object sender, EventArgs e)
        {
            this.LoadData();
        }

        private void LoadData()
        {
            try
            {
                List<RemoteMachine> list = RemoteMachineHelper.Read();

                BindingSource bindingSource = new BindingSource();
                bindingSource.DataSource = list;
                if (this.txtName.Text != "")
                    bindingSource.Filter = "Name='" + this.txtName.Text + "'";

                this.dataGridView1.DataSource = bindingSource;
            }
            catch (Exception ex)
            {
                WinMessageBox.ShowError("加载配置信息异常：" + ex.Message);
            }

        }
    }
}
