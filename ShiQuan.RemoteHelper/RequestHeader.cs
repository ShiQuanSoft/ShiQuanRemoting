﻿using ShiQuan.Remoting.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.RemoteHelper
{
    /// <summary>
    /// 请求头
    /// </summary>
    public class RequestHeader : BasicEntity
    {
        /// <summary>
        /// 类型：text/html
        /// </summary>
        public string ContentType
        {
            get { return this.Get("ContentType"); }
            set { this.Set("ContentType", value); }
        }
        /// <summary>
        /// 内容编码：UTf-8
        /// </summary>
        public string Charset
        {
            get
            {
                var temp = this.Get("Charset");
                if (string.IsNullOrEmpty(temp))
                    temp = "utf-8";
                return temp;
            }
            set { this.Set("Charset", value); }
        }
        /// <summary>
        /// 内容长度
        /// </summary>
        public int ContentLength
        {
            get
            {
                int temp = 0;
                int.TryParse(this.Get("ContentLength"), out temp);
                return temp;
            }
            set { this.Set("ContentLength", value.ToString()); }
        }
        /// <summary>
        /// 调用方法
        /// </summary>
        public string Action
        {
            get { return this.Get("Action"); }
            set { this.Set("Action", value); }
        }

        /// <summary>
        /// 保存文件路径
        /// </summary>
        public string FilePath
        {
            get { return this.Get("FilePath"); }
            set { this.Set("FilePath", value); }
        }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName
        {
            get { return this.Get("FileName"); }
            set { this.Set("FileName", value); }
        }
    }
}
