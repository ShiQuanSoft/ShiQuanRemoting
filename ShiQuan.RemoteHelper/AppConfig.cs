﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ShiQuan.Remoting.Helper
{
    /// <summary>
    /// App 配置信息
    /// </summary>
    public class AppConfig
    {
        /// <summary>
        /// 配置文件
        /// </summary>
        public static readonly string FileName = AppDomain.CurrentDomain.BaseDirectory + "ShiQuan.RemoteHelper.dll.config";

        private static BasicEntity _AppSetting = new BasicEntity();
        /// <summary>
        /// 初始配置信息-当配置信息为空时，可以初始配置信息
        /// </summary>
        public static void InitConfig()
        {
            var fileContent = ShiQuan.RemoteHelper.Properties.Resources.ShiQuan_Remoting_Helper;
            System.IO.File.WriteAllBytes(AppConfig.FileName, fileContent);
        }
        /// <summary>
        /// 保存设置内容
        /// </summary>
        /// <returns></returns>
        public static bool Save()
        {
            try
            {
                //增加的内容写在appSettings段下 <add key="RegCode" value="0"/>  
                /*
                 * 2020-04-10 侯连文 不同的执行程序需要调用同一配置信息
                 * System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                 */
                var execConfig = AppDomain.CurrentDomain.BaseDirectory + "ShiQuan.RemoteHelper.dll";
                Configuration config = ConfigurationManager.OpenExeConfiguration(execConfig);//Settings1.Default.ConfigPath
                foreach (var item in _AppSetting.GetFieldValue())
                {
                    if (config.AppSettings.Settings[item.Key] == null)
                    {
                        config.AppSettings.Settings.Add(item.Key, item.Value);
                    }
                    else
                    {
                        config.AppSettings.Settings[item.Key].Value = item.Value;
                    }
                }
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");//重新加载新的配置文件
                /*清空设置信息*/
                AppConfig._AppSetting.Clear();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("static void SetValue(string key, string value)", ex);
                return false;
            }
        }
        /// <summary>  
        /// 写入值  
        /// </summary>  
        /// <param name="key"></param>  
        /// <param name="value"></param>  
        static void SetValue(string key, string value)
        {
            try
            {
                //增加的内容写在appSettings段下 <add key="RegCode" value="0"/>  
                /*
                 * 2020-04-10 侯连文 不同的执行程序需要调用同一配置信息
                 * System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                 */
                //Configuration config = ConfigurationManager.OpenExeConfiguration(AppDomain.CurrentDomain.BaseDirectory + "XHTJCUpload.Config.dll");//Settings1.Default.ConfigPath
                //if (config.AppSettings.Settings[key] == null)
                //{
                //    config.AppSettings.Settings.Add(key, value);
                //}
                //else
                //{
                //    config.AppSettings.Settings[key].Value = value;
                //}
                //config.Save(ConfigurationSaveMode.Modified);
                //ConfigurationManager.RefreshSection("appSettings");//重新加载新的配置文件
                /*2020-04-27 侯连文 改用批量保存，不使用设置一个值，保存一个值*/
                AppConfig._AppSetting.Set(key, value);
            }
            catch (Exception ex)
            {
                Logger.Error("static void SetValue(string key, string value)", ex);
            }
        }

        /// <summary>  
        /// 读取指定key的值  
        /// </summary>  
        /// <param name="key"></param>  
        /// <returns></returns>  
        static string GetValue(string key)
        {
            try
            {
                /*
                 * 2020-04-10 侯连文 不同的执行程序需要调用同一配置信息
                 * System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                 */
                if (System.IO.File.Exists(AppConfig.FileName) == false)
                {
                    var fileContent = ShiQuan.RemoteHelper.Properties.Resources.ShiQuan_Remoting_Helper;
                    System.IO.File.WriteAllBytes(AppConfig.FileName, fileContent);
                }
                var execConfig = AppDomain.CurrentDomain.BaseDirectory + "ShiQuan.RemoteHelper.dll";
                Configuration config = ConfigurationManager.OpenExeConfiguration(execConfig);//Settings1.Default.ConfigPath

                if (config.AppSettings.Settings[key] == null)
                    return "";
                else
                    return config.AppSettings.Settings[key].Value;
            }
            catch (Exception ex)
            {
                Logger.Error("static string GetValue(string key)", ex);
                return "";
            }
        }
        /// <summary>
        /// 服务器
        /// </summary>
        public static string Server { get { return GetValue("Server"); } set { SetValue("Server", value); } }

        /// <summary>
        /// 服务器端口
        /// </summary>
        public static string Port { get { return GetValue("Port"); } set { SetValue("Port", value); } }

        /// <summary>
        /// 本机端口
        /// </summary>
        public static string LocalPort { get { return GetValue("LocalPort"); } set { SetValue("LocalPort", value); } }
        /// <summary>
        /// 配置唯一标识
        /// </summary>
        public static string Password { get { return GetValue("Password"); } set { SetValue("Password", value); } }
    }
}
