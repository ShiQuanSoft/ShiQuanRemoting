﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.RemoteHelper
{
    /// <summary>
    /// 基础实体
    /// </summary>
    public class BasicEntity
    {
        private Dictionary<string, string> _fieldValue = new Dictionary<string, string>();
        /// <summary>
        /// 获取字段集合
        /// </summary>
        public Dictionary<string, string> GetFieldValue()
        {
            return this._fieldValue;
        }
        /// <summary>
        /// 清除字段内容
        /// </summary>
        public void Clear()
        {
            this._fieldValue.Clear();
        }
        /// <summary>
        /// 设置
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void Set(string name, string value)
        {
            if (this._fieldValue.ContainsKey(name))
                this._fieldValue[name] = value;
            else
                this._fieldValue.Add(name, value);
        }
        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string Get(string name)
        {
            if (this._fieldValue.ContainsKey(name))
                return this._fieldValue[name];
            return string.Empty;
        }
    }
}
