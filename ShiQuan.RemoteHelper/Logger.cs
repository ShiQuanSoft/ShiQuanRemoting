﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.Remoting.Helper
{
    /// <summary>
    /// 日志存储
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Info(string classify, string msg)
        {

            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                if (System.IO.Directory.Exists(filePath) == false)
                    System.IO.Directory.CreateDirectory(filePath);

                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine(msg);

                string fileName = System.IO.Path.Combine(filePath, DateTime.Now.ToString("yyyyMMdd") + "-" + classify + ".log");
                //删除历史文件
                string[] files = System.IO.Directory.GetFiles(filePath, "*-" + classify + ".log");
                foreach (var item in files)
                {
                    if (item != fileName)
                        System.IO.File.Delete(item);
                }
                System.IO.File.AppendAllText(fileName, fileContent.ToString());
            }
            catch (Exception ex)
            {
                //无权限操作时
                Logger.MyDocuments(msg, ex);
            }
        }
        /// <summary>
        /// 信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Info(string msg)
        {

            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                if (System.IO.Directory.Exists(filePath) == false)
                    System.IO.Directory.CreateDirectory(filePath);

                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine(msg);

                string fileName = System.IO.Path.Combine(filePath, DateTime.Now.ToString("yyyyMMdd") + "-info.log");
                //删除历史文件
                string[] files = System.IO.Directory.GetFiles(filePath, "*-info.log");
                foreach (var item in files)
                {
                    if (item != fileName)
                        System.IO.File.Delete(item);
                }
                System.IO.File.AppendAllText(fileName, fileContent.ToString());
            }
            catch (Exception ex)
            {
                //无权限操作时
                Logger.MyDocuments(msg, ex);
            }
        }
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="title"></param>
        /// <param name="ex"></param>
        public static void Error(string title, Exception ex)
        {
            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                if (System.IO.Directory.Exists(filePath) == false)
                    System.IO.Directory.CreateDirectory(filePath);

                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine(title);
                fileContent.AppendLine(ex.ToString());

                string fileName = System.IO.Path.Combine(filePath, DateTime.Now.ToString("yyyyMMdd") + "-error.log");
                //删除历史文件
                string[] files = System.IO.Directory.GetFiles(filePath, "*-error.log");
                foreach (var item in files)
                {
                    if (item != fileName)
                        System.IO.File.Delete(item);
                }
                System.IO.File.AppendAllText(fileName, fileContent.ToString());
            }
            catch
            {
                //无权限操作时
                Logger.MyDocuments(title, ex);
            }
        }
        /// <summary>
        /// 异常
        /// </summary>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        public static void Error(string title, string msg)
        {
            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log");
                if (System.IO.Directory.Exists(filePath) == false)
                    System.IO.Directory.CreateDirectory(filePath);

                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine(msg);

                string fileName = System.IO.Path.Combine(filePath, DateTime.Now.ToString("yyyyMMdd") + "-error.log");
                //删除历史文件
                string[] files = System.IO.Directory.GetFiles(filePath, "*-error.log");
                foreach (var item in files)
                {
                    if (item != fileName)
                        System.IO.File.Delete(item);
                }
                System.IO.File.AppendAllText(fileName, fileContent.ToString());
            }
            catch (Exception ex)
            {
                //无权限操作时
                Logger.MyDocuments(msg, ex);
            }
        }
        /// <summary>
        /// 将文本文档日志保存到MyDocuments中
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="ex"></param>
        public static void MyDocuments(string msg, Exception ex)
        {
            try
            {
                string filePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "log");
                if (System.IO.Directory.Exists(filePath) == false)
                    System.IO.Directory.CreateDirectory(filePath);

                StringBuilder fileContent = new StringBuilder();
                fileContent.AppendLine("====");
                fileContent.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                fileContent.AppendLine(msg);
                fileContent.AppendLine(ex.ToString());

                string fileName = System.IO.Path.Combine(filePath, DateTime.Now.ToString("yyyy-MM-dd") + "-error.log");
                //删除历史文件
                string[] files = System.IO.Directory.GetFiles(filePath, "*-error.log");
                foreach (var item in files)
                {
                    if (item != fileName)
                        System.IO.File.Delete(item);
                }
                System.IO.File.WriteAllText(fileName, fileContent.ToString());
            }
            catch
            {

            }
        }
    }
}
