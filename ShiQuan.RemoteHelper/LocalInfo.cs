﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.Remoting.Helper
{
    /// <summary>
    /// 本地信息
    /// </summary>
    public class LocalInfo : BasicEntity
    {
        /// <summary>
        /// KeyValue
        /// </summary>
        public string KeyValue
        {
            get { return this.Get("KeyValue"); }
            set { this.Set("KeyValue", value); }
        }
        /// <summary>
        /// 用户Id
        /// </summary>
        public string UserId
        {
            get { return this.Get("UserId"); }
            set { this.Set("UserId", value); }
        }
        /// <summary>
        /// SerialNumber
        /// </summary>
        public string SerialNumber {
            get { return this.Get("SerialNumber"); }
            set { this.Set("SerialNumber", value); }
        }
        /// <summary>
        /// MacAddress 地址
        /// </summary>
        public string MacAddress {
            get { return this.Get("MacAddress"); }
            set { this.Set("MacAddress", value); }
        }
        /// <summary>
        /// ProcessorId
        /// </summary>
        public string ProcessorId {
            get { return this.Get("ProcessorId"); }
            set { this.Set("ProcessorId", value); }
        }
        /// <summary>
        /// 本地主机
        /// </summary>
        public string RemoteMachine
        {
            get { return this.Get("RemoteMachine"); }
            set { this.Set("RemoteMachine", value); }
        }
        /// <summary>
        /// 本地端口
        /// </summary>
        public string Port
        {
            get { return this.Get("Port"); }
            set { this.Set("Port", value); }
        }
        /// <summary>
        /// Password
        /// </summary>
        public string Password
        {
            get { return this.Get("Password"); }
            set { this.Set("Password", value); }
        }
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime CloseTime
        {
            get
            {
                var temp = this.Get("CloseTime");
                DateTime date = DateTime.MinValue;
                DateTime.TryParse(temp, out date);
                return date;
            }
            set { this.Set("CloseTime", value.ToString("yyyy-MM-dd HH:mm:ss")); }
        }
    }
}
