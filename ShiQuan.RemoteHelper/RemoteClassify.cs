﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.Remoting.Helper
{
    /// <summary>
    /// 远程分类
    /// </summary>
    public class RemoteClassify:BasicEntity
    {
        /// <summary>
        /// 唯一标识
        /// </summary>
        public string Id
        {
            get { return this.Get("Id"); }
            set { this.Set("Id", value); }
        }
        /// <summary>
        /// 编号-用于排序
        /// </summary>
        public string Code
        {
            get { return this.Get("Code"); }
            set { this.Set("Code", value); }
        }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get { return this.Get("Name"); }
            set { this.Set("Name", value); }
        }

        /// <summary>
        /// 上级标识
        /// </summary>
        public string ParentId
        {
            get { return this.Get("Id"); }
            set { this.Set("Id", value); }
        }

        /// <summary>
        /// 备注说明
        /// </summary>
        public string Remark
        {
            get { return this.Get("Remark"); }
            set { this.Set("Remark", value); }
        }
    }
}
