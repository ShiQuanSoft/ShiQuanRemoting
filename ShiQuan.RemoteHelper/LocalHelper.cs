﻿using ShiQuan.RemoteHelper;
using ShiQuan.SmartDevice;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ShiQuan.Remoting.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public class LocalHelper
    {
        /// <summary>
        /// 本机标识
        /// </summary>
        public static string KeyValue = string.Empty;
        /// <summary>
        /// 本机密码
        /// </summary>
        public static string Password = string.Empty;
        /// <summary>
        /// 保存文件地址
        /// </summary>
        private static string SavePath
        {
            get { return LocalHelper.GetFilePath(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "savepath")); }
        }

        private static string GetFilePath(string filePath)
        {
            if (System.IO.Directory.Exists(filePath) == false)
                System.IO.Directory.CreateDirectory(filePath);
            return filePath;
        }
        /// <summary>
        /// 获取本地主机信息
        /// </summary>
        /// <returns></returns>
        public static LocalInfo GetLocalInfo()
        {
            LocalInfo localInfo = new LocalInfo();
            try
            {
                var hddInfo = AtapiDevice.GetHddInfo(0);
                localInfo.SerialNumber = hddInfo.SerialNumber;
            }
            catch (Exception ex)
            {
                Logger.Error("读取硬盘ID异常", ex);
            }
            Win32MachineHelper machineHelper = new Win32MachineHelper();
            try
            {
                if (string.IsNullOrEmpty(localInfo.SerialNumber))
                    localInfo.SerialNumber = machineHelper.GetSerialNumber();
                if (string.IsNullOrEmpty(localInfo.SerialNumber))
                    localInfo.SerialNumber = machineHelper.GetDiskDriveId();
                localInfo.MacAddress = machineHelper.GetMacAddress();
            }
            catch (Exception ex)
            {
                Logger.Error("读取MacAddress异常", ex);
            }
            return localInfo;
        }

        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool SendFile(Socket mClient, RequestHeader header, out string msg)
        {
            msg = string.Empty;

            //byte[] bytes = Convert.FromBase64String(sendFileInfo.Content);
            //var fileName = System.IO.Path.Combine(RemoteHelper.SavePath, sendFileInfo.FileName);
            //System.IO.File.WriteAllBytes(fileName, bytes);

            var savePath = LocalHelper.SavePath;
            if (string.IsNullOrEmpty(header.FilePath) == false && System.IO.Directory.Exists(header.FilePath))
                savePath = header.FilePath;
            var fileName = System.IO.Path.Combine(savePath, header.FileName);
            /*保存文件内容*/
            byte[] bytes = new byte[1024 * 1024];
            int start = 0;
            using (FileStream writer = File.Open(fileName, FileMode.Create, FileAccess.Write))
            {
                do
                {
                    int count = mClient.Receive(bytes);
                    if (count <= 0)
                        break;
                    writer.Write(bytes, 0, count);
                    start += count;
                } while (start < header.ContentLength);
                writer.Flush();
            }
            return true;
        }

    }
}
