﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.Remoting.Helper
{
    /// <summary>
    /// 请求结果
    /// </summary>
    public class RequestResult : BasicEntity
    {
        public string Code { get{ return this.Get("Code"); } set { this.Set("Code", value); } }

        public string Message { get { return this.Get("Message"); } set { this.Set("Message", value); } }

        public string Result { get { return this.Get("Result"); } set { this.Set("Result", value); } }
    }
}
