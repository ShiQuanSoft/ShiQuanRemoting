﻿using ShiQuan.Remoting.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.RemoteHelper
{
    /// <summary>
    /// 发送文件信息
    /// </summary>
    public class SendFileInfo : BasicEntity
    {
        /// <summary>
        /// 保存文件路径
        /// </summary>
        public string FilePath
        {
            get { return this.Get("FilePath"); }
            set { this.Set("FilePath", value); }
        }
        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName
        {
            get { return this.Get("FileName"); }
            set { this.Set("FileName", value); }
        }
        /// <summary>
        /// 大小
        /// </summary>
        public int Size
        {
            get
            {
                int result = 0;
                int.TryParse(Get("Size"), out result);
                return result;
            }
            set { this.Set("Size", value.ToString()); }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Content
        {
            get { return this.Get("Content"); }
            set { this.Set("Content", value); }
        }
    }
}
