﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace ShiQuan.RemoteHelper
{
    /// <summary>
    /// 序列化
    /// </summary>
    public class JsonHelper
    {
        /// <summary>
        /// Xml 序列化
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serialize(object obj)
        {
            byte[] bytes = new byte[0];
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                XmlSerializer xmlserializer = new XmlSerializer(obj.GetType());
                xmlserializer.Serialize(stream, obj);
                bytes = stream.ToArray();
            }
            return System.Text.Encoding.UTF8.GetString(bytes);
        }
        /// <summary>
        /// Xml 反序列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string json)
        {
            T obj;
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(json);
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream(bytes))
            {
                XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
                obj = (T)xmlserializer.Deserialize(stream);
            }
            return obj;
        }

        /// <summary>
        /// JSON序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static string ToJson<T>(T t)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(t);
        }
        /// <summary>
        /// JSON序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static void Save<T>(T t, string fileName)
        {
            var jsonString = JsonHelper.ToJson(t);
            System.IO.File.WriteAllText(fileName, jsonString, System.Text.Encoding.UTF8);
        }
        /// <summary>
        /// JSON反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonSring"></param>
        /// <returns></returns>
        public static T GetByFile<T>(string fileName)
        {
            if (System.IO.File.Exists(fileName) == false)
                return default(T);
            var jsonString = System.IO.File.ReadAllText(fileName, System.Text.Encoding.UTF8);
            return JsonHelper.GetObject<T>(jsonString);
        }
        /// <summary>
        /// JSON反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonSring"></param>
        /// <returns></returns>
        public static T GetObject<T>(string jsonString)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Deserialize<T>(jsonString);
        }
    }
}
