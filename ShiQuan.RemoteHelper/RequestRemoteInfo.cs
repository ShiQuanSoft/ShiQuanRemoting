﻿using ShiQuan.Remoting.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.RemoteHelper
{
    /// <summary>
    /// 请求信息
    /// </summary>
    public class RequestRemoteInfo : BasicEntity
    {
        /// <summary>
        /// 用户标识
        /// </summary>
        public string UserId
        {
            get { return this.Get("UserId"); }
            set { this.Set("UserId", value); }
        }
        /// <summary>
        /// 请求者
        /// </summary>
        public string RequestId
        {
            get { return this.Get("RequestId"); }
            set { this.Set("RequestId", value); }
        }
        /// <summary>
        /// 远程者
        /// </summary>
        public string RemoteId
        {
            get { return this.Get("RemoteId"); }
            set { this.Set("RemoteId", value); }
        }
    }
}
