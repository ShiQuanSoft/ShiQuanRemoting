﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.RemoteHelper
{
    /// <summary>
    /// 远程主机配置信息
    /// </summary>
    public class RemoteMachine : BasicEntity
    {
        public RemoteMachine()
        {
            this.Port = "3389";
            this.Account = "administrator";
        }
        /// <summary>
        /// 唯一标识
        /// </summary>
        public string Id
        {
            get { return this.Get("Id"); }
            set { this.Set("Id", value); }
        }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get { return this.Get("Name"); }
            set { this.Set("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Server
        {
            get { return this.Get("Server"); }
            set { this.Set("Server", value); }
        }
        /// <summary>
        /// 端口
        /// </summary>
        public string Port
        {
            get { return this.Get("Port"); }
            set { this.Set("Port", value); }
        }
        /// <summary>
        /// 登录类型
        /// </summary>
        public string LoginType
        {
            get { return this.Get("LoginType"); }
            set { this.Set("LoginType", value); }
        }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string Account
        {
            get { return this.Get("Account"); }
            set { this.Set("Account", value); }
        }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password
        {
            get { return this.Get("Password"); }
            set { this.Set("Password", value); }
        }
        /// <summary>
        /// 桌面宽度
        /// </summary>
        public string DesktopWidth
        {
            get { return this.Get("DesktopWidth"); }
            set { this.Set("DesktopWidth", value); }
        }
        /// <summary>
        /// 桌面高度
        /// </summary>
        public string DesktopHeight
        {
            get { return this.Get("DesktopHeight"); }
            set { this.Set("DesktopHeight", value); }
        }
        /// <summary>
        /// rdesktop.rdp
        /// </summary>
        public string RdesktopFile
        {
            get { return this.Get("RdesktopFile"); }
            set { this.Set("RdesktopFile", value); }
        }
        /// <summary>
        /// 备注说明
        /// </summary>
        public string Remark
        {
            get { return this.Get("Remark"); }
            set { this.Set("Remark", value); }
        }
    }
}
