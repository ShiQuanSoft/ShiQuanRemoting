﻿using ShiQuan.RemoteHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ShiQuan.RemoteServer
{
    /// <summary>
    /// 基础业务处理
    /// </summary>
    public interface IBaseAction
    {
        /// <summary>
        /// 处理
        /// </summary>
        /// <param name="xml"></param>
        void Business(SocketServer server, Socket client, RequestHeader header);
    }
}
