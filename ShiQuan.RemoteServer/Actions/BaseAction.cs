﻿using ShiQuan.RemoteHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ShiQuan.RemoteServer
{
    /// <summary>
    /// 基础处理方法
    /// </summary>
    public abstract class BaseAction : IBaseAction
    {
        /// <summary>
        /// 业务处理
        /// </summary>
        /// <param name="xml"></param>
        public abstract void Business(SocketServer server, Socket client , RequestHeader header);

        /// <summary>
        /// 获取Body
        /// </summary>
        /// <param name="client"></param>
        /// <param name="contentLength"></param>
        /// <returns></returns>
        protected virtual string ReadBody(Socket client, RequestHeader header)
        {
            StringBuilder body = new StringBuilder();
            byte[] bytes = new byte[1024 * 1024];
            int start = 0;
            do
            {
                int count = client.Receive(bytes);
                if (count == 0)
                    break;

                body.Append(System.Text.Encoding.GetEncoding(header.Charset).GetString(bytes, 0, count));
                start += count;
            }
            while (start < header.ContentLength);
            return body.ToString();
        }
    }
}
