﻿using ShiQuan.RemoteHelper;
using ShiQuan.Remoting.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace ShiQuan.RemoteServer
{
    /// <summary>
    /// 发送文件
    /// </summary>
    public class SendFileAction : BaseAction
    {
        /// <summary>
        /// 业务处理
        /// </summary>
        /// <param name="server"></param>
        /// <param name="client"></param>
        /// <param name="jsonData"></param>
        public override void Business(SocketServer server, Socket client, RequestHeader header)
        {
            try
            {
                //var jsonData = this.ReadBody(client, header);
                //SendFileInfo sendFileInfo = JsonHelper.GetObject<SendFileInfo>(jsonData);
                string msg = string.Empty;
                if (LocalHelper.SendFile(client, header, out msg) == false)
                {
                    server.Fail(client, "保存文件失败");
                    return;
                }
                server.Success(client, "保存文件异常成功");
            }
            catch (Exception ex)
            {
                Logger.Error("保存文件异常", ex);
                server.Fail(client, "保存文件异常");
            }
        }
    }
}
