﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShiQuan.RdpDesktop
{
    /// <summary>
    /// 远程桌面信息
    /// </summary>
    public class RDesktopInfo
    {
        private string _FullAddress = string.Empty;
        /// <summary>
        /// 远程地址
        /// </summary>
        public string FullAddress {
            get { return this._FullAddress; }
            set { this._FullAddress = value; }
        }
        /// <summary>
        /// 登录账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; set; }
        private string _DesktopWidth = string.Empty;
        /// <summary>
        /// 桌面宽度
        /// </summary>
        public string DesktopWidth { 
            get { 
                if(string.IsNullOrEmpty(this._DesktopWidth))
                    this._DesktopWidth = "1440";
                return this._DesktopWidth; } 
            set { this._DesktopWidth = value; } 
        }
        private string _DesktopHeight = string.Empty;
        /// <summary>
        /// 桌面高度
        /// </summary>
        public string DesktopHeight
        {
            get
            {
                if (string.IsNullOrEmpty(this._DesktopHeight))
                    this._DesktopHeight = "860";
                return this._DesktopHeight;
            }
            set { this._DesktopHeight = value; }
        }
        
    }
}
