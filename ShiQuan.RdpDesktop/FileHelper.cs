﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ShiQuan.RdpDesktop
{
    /// <summary>
    /// 文件辅助对象
    /// </summary>
    public class FileHelper
    {
        /// <summary>
        /// 创建或更新文件
        /// </summary>
        /// <param name="fileContent">文件内容</param>
        /// <param name="filePath">路径</param>
        public static void SaveFile(string filePath,string fileContent)
        {
            if (!File.Exists(filePath))
            {
                using (FileStream fs1 = new FileStream(filePath, FileMode.Create, FileAccess.Write))//创建写入文件
                {
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.WriteLine(fileContent);//开始写入值
                    sw.Close();
                    fs1.Close();
                }
            }
            else
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Write))
                {
                    StreamWriter sr = new StreamWriter(fs);
                    sr.WriteLine(fileContent);//开始写入值
                    sr.Close();
                    fs.Close();
                }
            }
        }

        /// <summary>
        /// 创建或更新文件
        /// </summary>
        /// <param name="fileContent">文件内容</param>
        /// <param name="filePath">路径</param>
        public static void SaveFile(string filePath,byte[] fileBytes)
        {
            if (!File.Exists(filePath))
            {
                using (FileStream fs1 = new FileStream(filePath, FileMode.Create, FileAccess.Write))//创建写入文件
                {
                    StreamWriter sw = new StreamWriter(fs1);
                    sw.Write(fileBytes);//开始写入值
                    sw.Close();
                    fs1.Close();
                }
            }
            else
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Write))
                {
                    StreamWriter sr = new StreamWriter(fs);
                    sr.Write(fileBytes);//开始写入值
                    sr.Close();
                    fs.Close();
                }
            }
        }
    }
}
